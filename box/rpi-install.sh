#!/bin/bash

# Run this script as root ie:
# wget -O - https://bitbucket.org/ollomind/scripts/box/raw/master/rpi-install.sh | sudo bash

set -e

bold=`tput bold`;
normal=`tput sgr0`;
username="pi"
space_left=`df | grep rootfs | awk '{print $3}'`


if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

if [[ $space_left -lt 100000 ]]
then
	echo "${bold} In order to install the ollo software, you must have at least 100 megs of free space. Try running raspi-config and using the \"expand_rootfs\" option to free up some space ${normal}"
	exit 1
fi

# Updating apt-get
echo -e "\n→ ${bold}Updating apt-get${normal}\n";
sudo apt-get update > /dev/null;

echo -e "\n→ ${bold}Installing python-software-properties${normal}\n";
sudo apt-get install python-software-properties > /dev/null;

echo -e "\n→ ${bold}Installing ntpdate${normal}\n";
sudo apt-get -qq -y -f -m install ntpdate > /dev/null;
sudo /etc/init.d/ntp stop

# Add NTP Update as a daily cron job
echo -e "\n→ ${bold}Create the ntpdate file${normal}\n";
sudo touch /etc/cron.daily/ntpdate;
echo -e "\n→ ${bold}Add ntpdate ntp.ubuntu.com${normal}\n";
sudo echo "ntpdate ntp.ubuntu.com" > /etc/cron.daily/ntpdate;
echo -e "\n→ ${bold}Making ntpdate executable${normal}\n";
sudo chmod 755 /etc/cron.daily/ntpdate;


# Update the timedate
echo -e "\n→ ${bold}Updating the time${normal}\n";
sudo ntpdate ntp.ubuntu.com pool.ntp.org;
sudo /etc/init.d/ntp start

###################################################################
# Download and install the Essential packages
###################################################################

echo -e "\n→ ${bold}Installing avahi-daemon${normal}\n";
sudo apt-get -qq -y -f -m  install avahi-daemon > /dev/null;

echo -e "\n→ ${bold}Installing upstart${normal}\n";
echo 'Yes, do as I say!' | sudo apt-get -o DPkg::options=--force-remove-essential -y --force-yes install upstart > /dev/null;

# Download and install the Essential packages.
echo -e "\n→ ${bold}Installing git${normal}\n";
sudo apt-get -qq -y -f -m  install git > /dev/null;

echo -e "\n→ ${bold}Installing node & npm${normal}\n";
cd /tmp
rm -f node_latest_armhf.*
# wget http://nodejs.org/dist/latest/node-v0.10.26-linux-arm-pi.tar.gz
# cd /usr/local
# sudo tar xzvf ~/node-v0.10.26-linux-arm-pi.tar.gz  --strip=1
wget http://node-arm.herokuapp.com/node_latest_armhf.deb > /dev/null;
sudo dpkg -i node_latest_armhf.deb > /dev/null;
rm -f node_latest_armhf.*

echo -e "\n→ ${bold}Installing ruby1.9.1-dev${normal}\n";
sudo apt-get -qq -y -f -m  install ruby1.9.1-dev > /dev/null;

echo -e "\n→ ${bold}Installing avrdude${normal}\n";
sudo apt-get -qq -y -f -m  install avrdude > /dev/null;

echo -e "\n→ ${bold}Installing psmisc${normal}\n";
sudo apt-get -qq -y -f -m  install psmisc > /dev/null;

echo -e "\n→ ${bold}Installing curl${normal}\n";
sudo apt-get -qq -y -f -m  install curl > /dev/null;


# Install Sinatra
echo -e "\n→ ${bold}Installing the sinatra gem${normal}\n";
sudo gem install sinatra  --verbose --no-rdoc --no-ri > /dev/null;

# Install getifaddrs
echo -e "\n→ ${bold}Installing the getifaddrs gem${normal}\n";
sudo gem install system-getifaddrs  --verbose --no-rdoc --no-ri > /dev/null;

###################################################################
# Download and install ollo packages
###################################################################

echo -e "\n→ ${bold}Create the Ollo Box root Folder${normal}\n";
sudo mkdir -p  /opt/ollo;

# Set user as the owner of /opt/ollo.
echo -e "\n→ ${bold}Set ${username} user as the owner of /opt/ollo${normal}\n";
sudo chown -R ${username}:${username} /opt/ollo

echo -e "\n→ ${bold}Add user to sudoers${normal}\n";
sudo echo "pi ALL=NOPASSWD: /sbin/ifconfig, /sbin/iwlist" > /etc/sudoers.d/ollo
sudo chmod 0440 /etc/sudoers.d/ollo

# Create the Ollo Box tools folder
echo -e "\n→ ${bold}Create the Ollo Box Tools Folder${normal}\n"; 
sudo mkdir -p  /opt/ollo/tools;

# Clone the Ollo Tools into /opt/ollo/tools
echo -e "\n→ ${bold}Fetching the Tools Repo from Github${normal}\n";
git clone https://bitbucket.org/ollomind/tools.git /opt/ollo/tools > /dev/null;
cd /opt/ollo/tools;
git checkout master; #this will change once release is finished


# Copy /etc/udev/rules.d/ scripts into place (for web cams)
# echo -e "\n→ ${bold}Copy /etc/udev/rules.d/ scripts into place${normal}\n";
# sudo cp /opt/ollo/tools/udev/* /etc/udev/rules.d/;


# Create Ollo Agent Directory (-p to preserve if already exists).
echo -e "\n→ ${bold}Create the Ollo Agent Directory${normal}\n";
sudo mkdir -p /opt/ollo/agent;

# Clone the Ollo Agent into opt
echo -e "\n→ ${bold}Clone the Ollo Agent into opt${normal}\n";
git clone https://bitbucket.org/ollomind/agent.git /opt/ollo/agent > /dev/null;
cd /opt/ollo/agent;
git checkout master;

sudo rm -fr /root/tmp
echo -e "\n→ ${bold}Installing packages for agent${normal}\n";
sudo bash bin/install.sh > /dev/null;

# echo -e "${bold}Pulling down RPI binaries${normal}";
# cd /tmp;
# wget https://s3.amazonaws.com/olloblocks/binaries/pi/rpi-binaries.tgz > /dev/null;
# echo -e "${bold}Deploying RPI binaries${normal}";
# tar -C / -xzf rpi-binaries.tgz > /dev/null;
# rm rpi-binaries.tgz;
# sudo ln -s /opt/ollo/agent/node_modules/forever/bin/forever /usr/bin/forever

# echo -e "${bold}Install node packages${normal}";

# Clone the Ollo Box into opt
echo -e "\n→ ${bold}Clone the Ollo Box into opt${normal}\n";
git clone https://bitbucket.org/ollomind/box.git /opt/ollo/box > /dev/null;
cd /opt/ollo/box;
git checkout master;

npm install > /dev/null;


# Create directory /etc/opt/ollo
echo -e "\n→ ${bold}Adding /etc/opt/ollo${normal}\n";
sudo mkdir -p /etc/opt/ollo;

# Set user as the owner of /etc/opt/ollo.
echo -e "\n→ ${bold}Set ${username} user as the owner of /etc/opt/ollo${normal}\n";
sudo chown -R ${username} /etc/opt/ollo;

# Set user as the owner of this directory.
# echo -e "\n→ ${bold}Set ${username} user as the owner of this directory${normal}\n";
# sudo chown -R ${username} /opt/;


# Copy /etc/init scripts into place
echo -e "\n→ ${bold}Copy /etc/init scripts into place${normal}\n";
sudo cp /opt/ollo/tools/init/* /etc/init/

# Set the correct owner and permissions on the files
echo -e "\n→ ${bold}Set the correct owner and permissions on the init files${normal}\n";
sudo chown root:root /etc/init/*;
sudo chmod 644 /etc/init/*;

echo -e "\n→ ${bold}Set the correct owner and permissions on /var/log${normal}\n";
sudo chown -R ${username}:${username} /var/log


# Add /opt/ollo/tools/bin to root's path
echo -e "\n→ ${bold}Adding /opt/ollo/tools/bin to root's path${normal}\n";
echo 'export PATH=/opt/ollo/tools/bin:$PATH' >> /root/.bashrc;

# Add /opt/ollo/tools/bin to user's path
echo -e "\n→ ${bold}Adding /opt/ollo/tools/bin to ${username}'s path${normal}\n";
echo 'export PATH=/opt/ollo/tools/bin:$PATH' >> /home/${username}/.bashrc;

# Set the box's environment
echo -e "\n→ ${bold}Setting the box's environment to stable${normal}\n";
echo 'export OLLO_ENV=stable' >> /home/${username}/.bashrc;

echo -e "\n→ ${bold}Generating serial number from system${normal}\n";
sudo bash /opt/ollo/tools/bin/rpi/sn;

echo "agent=\"pi\"" >> /etc/environment.local

echo -e "\n→ ${bold}Guess what? We're done! ${normal}\n";

echo -e "Before you reboot, write down this serial-- this is what you will need to activate your new Box!"

echo -e "--------------------------------------------------------------"
echo -e "|                                                            |"
echo -e "|           Your OlloPi Serial is: `cat /etc/opt/ollo/serial.conf`          |"
echo -e "|                                                            |"
echo -e "--------------------------------------------------------------"

read -p " When you are ready, please hit the [Enter] key" nothing

if [[ ! -d /etc/update-motd.d ]]
then
	sudo mkdir /etc/update-motd.d
fi

sudo bash /opt/ollo/tools/bin/update-system;
